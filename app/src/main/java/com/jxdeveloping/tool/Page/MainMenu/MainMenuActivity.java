package com.jxdeveloping.tool.Page.MainMenu;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import com.framework.MidLayer_Net.Listener.JSONRequestListener;
import com.framework.MidLayer_Net.Model.ErrorMsg;
import com.jxdeveloping.tool.Page.Login.LoginActivity;
import com.jxdeveloping.tool.Page.Main.MainActivity;
import com.lk.controller.Constants.MDFonts;
import com.lk.controller.Model.Http.CheckUpdataResponse;
import com.lk.controller.Model.Version;
import com.lk.controller.Page.BaseActivity;
import com.lk.controller.Sharepreference.SpConnecter;
import com.jxdeveloping.tool.Http.SettingHttp;
import com.jxdeveloping.tool.R;
import com.jxdeveloping.tool.Updata.DownAPKService;
import com.jxdeveloping.tool.Updata.UpdataDialog;
import com.jxdeveloping.tool.databinding.ActivityMainMenuBinding;


public class MainMenuActivity extends BaseActivity implements MainMenuAdapter.OnMainMenuItemClickListener, UpdataDialog.OnUpdataDialogClickListener {
    private static final MainMenuItem[] ITEMS = new MainMenuItem[]{
            new MainMenuItem(MDFonts.SEND, "单号扫描"),
    };

    private ActivityMainMenuBinding binding;

    private UpdataDialog updataDialog;

    private SettingHttp settingHttp;

    private String newVersionApkUrl;
    private boolean isFirstOpen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main_menu);
        setSupportActionBar(binding.toolbar);
        setTitle("功能选择");

//        settingHttp = new SettingHttp(this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        binding.recyclerView.setLayoutManager(gridLayoutManager);

        MainMenuAdapter mainMenuAdapter = new MainMenuAdapter(this);
        mainMenuAdapter.setMainMenuItems(ITEMS);
        binding.recyclerView.setAdapter(mainMenuAdapter);

        String token = getToken();
        if (token == null) {
            gotoLogin(LoginActivity.class);
        } else {
            this.onItemClick(binding.recyclerView, 0);
        }
        isFirstOpen = true;

        checkVersion();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        settingHttp.stopCheckUpdata();
    }

    private void showUpdataDialog(Version nowVersion, Version newVersion) {
        if (updataDialog == null) {
            updataDialog = new UpdataDialog(this, this);
        }
        updataDialog.setVersion(nowVersion, newVersion);
        updataDialog.show();
    }

    private void checkVersion() {
        settingHttp.checkUpdata(checkUpdataResponseJSONRequestListener);
    }

    /**
     * 是否开启登录检查
     *
     * @return
     */
    public boolean checkLogin() {
        if (isFirstOpen) {
            isFirstOpen = false;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        startActivity(new Intent(this, MainActivity.class).putExtra("intent", position));
        //finish();
    }

    public void onLogoutClick(View view) {
        clearToken();
        gotoLogin(LoginActivity.class);
    }

    @Override
    public void onUpdateNowClick() {
        Intent intent = new Intent(this, DownAPKService.class);
        intent.putExtra("apkUrl", newVersionApkUrl);
        startService(intent);
    }

    @Override
    public void onDontUpdateClick() {

    }

    @Override
    public void onLoginFailedCB() {
        super.onLoginFailedCB();
        finish();
    }

    private JSONRequestListener<CheckUpdataResponse> checkUpdataResponseJSONRequestListener = new JSONRequestListener<CheckUpdataResponse>() {
        @Override
        public void onJSONResponse(CheckUpdataResponse checkUpdataResponse) {
            if (checkUpdataResponse.isSuccess()) {
                SpConnecter spConnecter = new SpConnecter(MainMenuActivity.this, "version");
                Version nowVersion = spConnecter.loadObject(Version.class);
                if (nowVersion.getVersionCode() < checkUpdataResponse.getMap().getVersionCode()) {
                    newVersionApkUrl = checkUpdataResponse.getMap().getFileName();
                    showUpdataDialog(nowVersion, checkUpdataResponse.getMap());
                }
            }
        }

        @Override
        public void onError(int code, ErrorMsg errorMsg) {

        }
    };
}
