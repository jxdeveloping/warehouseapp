package com.jxdeveloping.tool.Page.MainMenu;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lk.controller.Constants.MDFontsPath;
import com.lk.controller.Font.FontLoader;
import com.jxdeveloping.tool.R;
import com.jxdeveloping.tool.databinding.ItemMainMenuBinding;


public class MainMenuAdapter extends RecyclerView.Adapter {
    private MainMenuItem[] mainMenuItems;

    private OnMainMenuItemClickListener onMainMenuItemClickListener;

    public MainMenuAdapter(@NonNull OnMainMenuItemClickListener onMainMenuItemClickListener) {
        this.onMainMenuItemClickListener = onMainMenuItemClickListener;
    }

    public void setMainMenuItems(MainMenuItem[] mainMenuItems) {
        this.mainMenuItems = mainMenuItems;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder((ItemMainMenuBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_main_menu, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        Holder holder = (Holder) viewHolder;
        holder.binding.setMenuItem(mainMenuItems[position]);
        FontLoader.setOcticons(MDFontsPath.PATH, holder.binding.textViewIcon);
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return mainMenuItems == null ? 0 : mainMenuItems.length;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onMainMenuItemClickListener.onItemClick(v, (Integer) v.getTag());
        }
    };

    private class Holder extends RecyclerView.ViewHolder {
        private ItemMainMenuBinding binding;

        public Holder(ItemMainMenuBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            itemView.setOnClickListener(onClickListener);
        }
    }

    public interface OnMainMenuItemClickListener {
        public void onItemClick(View view, int position);
    }
}
