package com.jxdeveloping.tool.Page.Login;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.framework.MidLayer_Net.Listener.JSONRequestListener;
import com.framework.MidLayer_Net.Model.ErrorMsg;
import com.jxdeveloping.tool.Http.UserHttp;
import com.lk.controller.Constants.HttpConstants;
import com.lk.controller.Constants.SpConstants;
import com.lk.controller.Model.Http.LoginResponse;
import com.lk.controller.Model.UserCache;
import com.lk.controller.Page.BaseActivity;
import com.lk.controller.Sharepreference.SpConnecter;

import com.jxdeveloping.tool.R;
import com.jxdeveloping.tool.databinding.ActivityLoginBinding;

import java.util.logging.Logger;


public class LoginActivity extends BaseActivity {
    private String TAG = LoginActivity.class.getName();
    private ActivityLoginBinding binding;

    private UserHttp userHttp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        userHttp = new UserHttp(this);

        binding.spinnerServer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(adapterView.getContext(),
//                        "OnItemSelectedListener : " + adapterView.getItemAtPosition(i).toString(),
//                        Toast.LENGTH_SHORT).show();
                String name = adapterView.getItemAtPosition(i).toString();
                if ("服务器1".equals(name)) {
                    HttpConstants.setHttpServerUrl("http://61.216.178.44:8000");
                } else if ("服务器2".equals(name)) {
                    Toast.makeText(adapterView.getContext(),
                            "未设置服务器地址",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    /**
     * 是否开启登录检查
     *
     * @return
     */
    public boolean checkLogin() {
        return false;
    }

    /**
     * 保存token
     *
     * @param userCache
     */
    public void saveToken(UserCache userCache) {
        SpConnecter spConnecter = new SpConnecter(this, SpConstants.USER_CACHE);
        if (userCache == null) {
            spConnecter.delete();
        } else {
            spConnecter.saveObject(userCache);
        }
    }

    public void onLoginClick(View view) {
        String userName = binding.editTextAccount.getText().toString();

        if (userName.length() == 0) {
            showSnakerBar("用户名不能为空");
            return;
        }
        String password = binding.editTextPwd.getText().toString();
        if (password.length() == 0) {
            showSnakerBar("请输入密码");
            return;
        }

        showProgressDialog();
        userHttp.login(userName, password, loginResponseJSONRequestListener);
    }

    private JSONRequestListener<LoginResponse> loginResponseJSONRequestListener = new JSONRequestListener<LoginResponse>() {
        @Override
        public void onJSONResponse(LoginResponse loginResponse) {
            hideProgressDialog();
            //loginResponse.setSuccess(true);
            //loginResponse.setToken("8b2ba1878a4949bfbfea296bbcb25783");
            if (loginResponse.isSuccess()) {
                saveToken(loginResponse.getUserCache());
                setResult(RESULT_OK);
                finish();
            } else {
                showSnakerBar(loginResponse.getMessage());
            }
        }

        @Override
        public void onError(int code, ErrorMsg errorMsg) {
            hideProgressDialog();
            showSnakerBar(errorMsg.getErrorInfo());
        }
    };
}
