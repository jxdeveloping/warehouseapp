package com.jxdeveloping.tool.Page.Send;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jxdeveloping.tool.R;
import com.lk.controller.Model.Product;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.VH> {
    private static String TAG = ProductAdapter.class.getName();
    private LayoutInflater mInflater;

    private List<Product> mItems = new ArrayList<>();

    public ProductAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public void setDatas(List<Product> beans) {
        mItems.clear();
        mItems.addAll(beans);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.fragment_send_item, parent, false);
        final VH holder = new VH(view);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mClickListener != null) {
//                    mClickListener.onItemClick(holder.getAdapterPosition(), v, holder);
//                }
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        Product item = mItems.get(position);

        holder.tvProductName .setText(item.getProductName());
        holder.tvWowcherCode.setText(item.getWowcherCode());
        holder.tvRedeemedAt.setText(item.getRedeemedAt());
        holder.tvOrderStatus.setText(item.getOrderStatus());
        holder.tvColour.setText(item.getColour());
//        URL picUrl;
//        try {
//            picUrl = new URL(item.getImageURL());
//            Bitmap pngBM = BitmapFactory.decodeStream(picUrl.openStream());
//            holder.imgAvatar.setImageBitmap(pngBM);
//        } catch (Exception e) {
//            Log.e(TAG, e.getMessage());
//        }
    }

    class VH extends RecyclerView.ViewHolder {
        private ImageView imgAvatar;
        private TextView tvProductName, tvWowcherCode, tvRedeemedAt, tvOrderStatus, tvColour;

        public VH(View itemView) {
            super(itemView);
            imgAvatar = (ImageView) itemView.findViewById(R.id.imageView_imageUrl);
            tvProductName = (TextView) itemView.findViewById(R.id.textView_productName);
            tvWowcherCode = (TextView) itemView.findViewById(R.id.textView_wowcherCode);
            tvRedeemedAt = (TextView) itemView.findViewById(R.id.textView_redeemedAt);
            tvOrderStatus = (TextView) itemView.findViewById(R.id.textView_orderStatus);
            tvColour = (TextView) itemView.findViewById(R.id.textView_colour);
        }
    }
}
