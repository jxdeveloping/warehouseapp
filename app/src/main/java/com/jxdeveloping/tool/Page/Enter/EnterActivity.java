package com.jxdeveloping.tool.Page.Enter;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.jxdeveloping.tool.Page.MainMenu.MainMenuActivity;
import com.lk.controller.Constants.SoundPath;
import com.lk.controller.Model.Version;
import com.lk.controller.Page.BaseActivity;
import com.lk.controller.Sharepreference.SpConnecter;
import com.lk.controller.Sound.SoundPlayer;
import com.jxdeveloping.tool.R;
import com.jxdeveloping.tool.databinding.ActivityEnterBinding;


public class EnterActivity extends BaseActivity {
    private ActivityEnterBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_enter);

        //检查是否安装了新版本
        try {
            initVersion();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //加载声音
        initSound();

        int permission = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        } else {
            initFinish();
        }
    }

    private void initVersion() throws PackageManager.NameNotFoundException {
        boolean alreadyUpdata = false;

        SpConnecter spConnecter = new SpConnecter(this, "version");
        Version lastVersion = spConnecter.loadObject(Version.class);

        Version nowVersion = new Version();
        PackageManager pm = getPackageManager();
        PackageInfo packageInfo = pm.getPackageInfo(getPackageName(), 0);
        nowVersion.setVersionCode(packageInfo.versionCode);
        nowVersion.setVersionName(packageInfo.versionName);

        if (lastVersion == null) {
            alreadyUpdata = true;
        } else if (lastVersion.getVersionCode() < nowVersion.getVersionCode()) {
            alreadyUpdata = true;
        }

        if (alreadyUpdata) {
            spConnecter.saveObject(nowVersion);
        }
    }

    private void initSound() {
        SoundPlayer.loadSound(getAssets(), SoundPath.SOUND_SUCCESS);
        SoundPlayer.loadSound(getAssets(), SoundPath.SOUND_FAILED);
    }

    private void initFinish() {
        //        startActivity(new Intent(this, BTClient.class));
//        startActivity(new Intent(this, MainActivity.class));
        startActivity(new Intent(this, MainMenuActivity.class));
//        startActivity(new Intent(this, LoginActivity.class));

        finish();
    }

    /**
     * 是否开启登录检查
     *
     * @return
     */
    public boolean checkLogin() {
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 0) {
            initFinish();
        }
    }
}
