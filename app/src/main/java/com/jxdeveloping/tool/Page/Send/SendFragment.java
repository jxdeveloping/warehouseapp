package com.jxdeveloping.tool.Page.Send;

import android.Manifest;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.framework.MidLayer_Net.Listener.JSONRequestListener;
import com.framework.MidLayer_Net.Model.ErrorMsg;
import com.google.zxing.common.StringUtils;
import com.jxdeveloping.tool.Http.WorkHttp;
import com.lk.controller.Model.Http.ProductInfoResponse;
import com.lk.controller.Model.Http.Response;
import com.lk.controller.Model.Http.SendResponse;
import com.lk.controller.Model.ProductInfo;
import com.lk.controller.Page.BaseFragment;

import com.jxdeveloping.tool.R;
import com.jxdeveloping.tool.databinding.FragmentSendBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;


public class SendFragment extends BaseFragment implements EasyPermissions.PermissionCallbacks {
    private static final int REQUEST_CODE_QRCODE_PERMISSIONS = 1;

    private FragmentSendBinding binding;

    private WorkHttp workHttp;

    private String deNo;

    private ProductAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        if (binding == null || binding.getRoot() == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_send, container, false);
        }
        binding.setSendFragment(this);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        workHttp = new WorkHttp(_mActivity);

    }


    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        super.onLazyInitView(savedInstanceState);
        binding.recy.setLayoutManager(new LinearLayoutManager(this.getContext()));
        mAdapter = new ProductAdapter(_mActivity);
        binding.recy.setAdapter(mAdapter);

        // mAdapter.setDatas(chatList);
    }


    private void send() {
        deNo = binding.editTextSendBatchId.getText().toString();
        if (deNo.length() == 0) {
            showSnakerBar("请输入单号");
            return;
        }

        showProgressDialog();
        workHttp.send(getToken(), deNo, sendRequestListener);
    }

    private TextWatcher boxIdEditorTextWatcher = new TextWatcher() {
        boolean canUpload = false;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            canUpload = false;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (start == 0 && before == 0 && count > 1) {
                canUpload = true;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (canUpload) {
                send();
            }
        }
    };

//    private TextView.OnEditorActionListener onBoxIdEditorActionListener = new TextView.OnEditorActionListener() {
//        @Override
//        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//            if (actionId == EditorInfo.IME_ACTION_DONE) {
//                send();
//            }
//            return false;
//        }
//    };

    public void onSendClick(View view) {
        send();
    }

    public void onScanQrcodeClick(View view) {
        requestCodeQRCodePermissions();
    }

    private JSONRequestListener<ProductInfoResponse> sendRequestListener = new JSONRequestListener<ProductInfoResponse>() {
        @Override
        public void onJSONResponse(ProductInfoResponse response) {
            hideProgressDialog();
            if (response.isSuccess()) {
                playSuccessSound();
                binding.editTextSendBatchId.setText(null);
                binding.editTextSendBatchId.requestFocus();
                showSnakerBar(response.getMessage());
                ProductInfo productInfo = response.getMap();
                String str = "  " + productInfo.getTrackingNo()
                +"    shipmentStatus:" +productInfo.getShipmentStatus() + "\r\n"
                        + "  shippingMethod : " + productInfo.getShippingMethod()
                        + "        agent : " + productInfo.getAgent();
                //信息.
                binding.textViewMainInfo.setText( str );
                mAdapter.setDatas(productInfo.getProductList());
                //binding.textViewResult.setText(productInfo.getTrackingNo());
            } else {
                playFailedSound();
                showSnakerBar(response.getMessage());
                // binding.textViewResult.setText(response.getMessage());
            }
        }

        @Override
        public void onError(int code, ErrorMsg errorMsg) {
            hideProgressDialog();
            playFailedSound();
            showSnakerBar(errorMsg.getErrorInfo());
        }
    };

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @AfterPermissionGranted(REQUEST_CODE_QRCODE_PERMISSIONS)
    private void requestCodeQRCodePermissions() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};
        if (!EasyPermissions.hasPermissions(this.getContext(), perms)) {
            EasyPermissions.requestPermissions(this, "扫描二维码需要打开相机和散光灯的权限", REQUEST_CODE_QRCODE_PERMISSIONS, perms);
            return;
        }
        startActivityForResult(new Intent(this.getContext(), BarcodeScanActivity.class), 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_OK:
                Bundle bundle = data.getExtras();
                String barcode = bundle.getString("barcode");
                if (!"".equals(barcode)) {
                    binding.editTextSendBatchId.setText(barcode);
                    send();
                }
                break;

            default:
                break;
        }
    }
}
