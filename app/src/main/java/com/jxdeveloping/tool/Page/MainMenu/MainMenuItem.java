package com.jxdeveloping.tool.Page.MainMenu;


public class MainMenuItem {
    private String iconText;
    private String text;

    public MainMenuItem(String iconText, String text) {
        this.iconText = iconText;
        this.text = text;
    }

    public String getIconText() {
        return iconText;
    }

    public void setIconText(String iconText) {
        this.iconText = iconText;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
