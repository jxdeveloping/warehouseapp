package com.jxdeveloping.tool.Page.Main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;
import com.framework.MidLayer_Net.Listener.JSONRequestListener;
import com.framework.MidLayer_Net.Model.ErrorMsg;
import com.jxdeveloping.tool.Http.SettingHttp;
import com.jxdeveloping.tool.Page.Home.HomeFragment;
import com.jxdeveloping.tool.Page.Login.LoginActivity;
import com.jxdeveloping.tool.Page.Send.SendFragment;
import com.jxdeveloping.tool.Updata.DownAPKService;
import com.jxdeveloping.tool.Updata.UpdataDialog;
import com.lk.controller.Model.Http.CheckUpdataResponse;
import com.lk.controller.Model.Version;
import com.lk.controller.Page.BaseActivity;
import com.lk.controller.Page.BaseFragment;
import com.lk.controller.Sharepreference.SpConnecter;

import com.jxdeveloping.tool.R;

import io.fabric.sdk.android.Fabric;


/**
 * 首页.
 */
public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener
        , UpdataDialog.OnUpdataDialogClickListener {
    // 再点一次退出程序时间设置
    private static final long WAIT_TIME = 2000L;
    private long TOUCH_TIME = 0;
    private SettingHttp settingHttp;

    private UpdataDialog updataDialog;

    private BaseFragment[] baseFragments = new BaseFragment[2];

    private String newVersionApkUrl;
    private DrawerLayout drawer;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //toolbar.setNavigationIcon(R.drawable.);

//        settingHttp = new SettingHttp(this);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_send);

        int intent = getIntent().getIntExtra("intent", 0);
        setupTitle(intent);

//        baseFragments[0] = findFragment(SendFragment.class);
//        if (baseFragments[0] == null) {
        baseFragments[0] = new HomeFragment();
        baseFragments[1] = new SendFragment();
            loadMultipleRootFragment(R.id.content, intent, baseFragments);
            showHideFragment(baseFragments[0]);
//        } else {
//            baseFragments[1] = findFragment(HomeFragment.class);
//            baseFragments[2] = findFragment(LoadFragment.class);
//            baseFragments[3] = findFragment(SendFragment.class);
//        }
    }

    private void showUpdateDialog(Version nowVersion, Version newVersion) {
        if (updataDialog == null) {
            updataDialog = new UpdataDialog(this, this);
        }
        updataDialog.setVersion(nowVersion, newVersion);
        updataDialog.show();
    }

    @Override
    public void onBackPressedSupport() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressedSupport();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_home:
                showHideFragment(baseFragments[0]);
                setupTitle(0);
                break;
            case R.id.nav_send:
                showHideFragment(baseFragments[1]);
                setupTitle(1);   break;
            case R.id.nav_logout:
                clearToken();
                gotoLogin(LoginActivity.class);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setupTitle(int intent) {
        switch (intent) {
            case 0:
                setTitle("首页");
                break;
            case 1:
                setTitle("单号扫描");

                break;
        }
    }

    @Override
    public void onUpdateNowClick() {
        Intent intent = new Intent(this, DownAPKService.class);
        intent.putExtra("apkUrl", newVersionApkUrl);
        startService(intent);
    }

    @Override
    public void onDontUpdateClick() {

    }

    private JSONRequestListener<CheckUpdataResponse> checkUpdataResponseJSONRequestListener = new JSONRequestListener<CheckUpdataResponse>() {
        @Override
        public void onJSONResponse(CheckUpdataResponse checkUpdataResponse) {
            hideProgressDialog();
            if (checkUpdataResponse.isSuccess()) {
                SpConnecter spConnecter = new SpConnecter(MainActivity.this, "version");
                Version nowVersion = spConnecter.loadObject(Version.class);
                if (nowVersion.getVersionCode() < checkUpdataResponse.getMap().getVersionCode()) {
                    newVersionApkUrl = checkUpdataResponse.getMap().getFileName();
                    showUpdateDialog(nowVersion, checkUpdataResponse.getMap());
                } else {
                    showSnakerBar("当前已是最新版本");
                }
            } else {
                showSnakerBar(checkUpdataResponse.getMessage());
            }
        }

        @Override
        public void onError(int code, ErrorMsg errorMsg) {
            hideProgressDialog();
            showSnakerBar(errorMsg.getErrorInfo());
        }
    };
}
