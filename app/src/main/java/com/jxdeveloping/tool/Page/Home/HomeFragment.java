package com.jxdeveloping.tool.Page.Home;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jxdeveloping.tool.Http.WorkHttp;
import com.jxdeveloping.tool.R;
import com.lk.controller.Page.BaseFragment;
import com.jxdeveloping.tool.databinding.FragmentHomeBinding;

public class HomeFragment extends BaseFragment {
    private FragmentHomeBinding binding;
    private WorkHttp workHttp;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        if (binding == null || binding.getRoot() == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        }
        binding.setHomeFragment(this);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        workHttp = new WorkHttp(_mActivity);
    }


}
