package com.jxdeveloping.tool.Application;

import android.app.Application;

import com.framework.MidLayer_Net.NetRequestUtils;



public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        NetRequestUtils.init(this);

        CrashHandler catchHandler = CrashHandler.getInstance();
        catchHandler.init(getApplicationContext());
    }
}
