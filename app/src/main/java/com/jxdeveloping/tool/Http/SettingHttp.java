package com.jxdeveloping.tool.Http;

import android.content.Context;

import com.framework.MidLayer_Net.Listener.JSONRequestListener;
import com.framework.MidLayer_Net.NetRequest;
import com.framework.MidLayer_Net.NetRequestUtils;
import com.lk.controller.Constants.HttpConstants;
import com.lk.controller.Http.BaseHttp;
import com.lk.controller.Model.Http.CheckUpdataResponse;

import java.util.HashMap;



public class SettingHttp extends BaseHttp {
    private NetRequest checkUpdataRequest;

    public SettingHttp(Context context) {
        super(context);
    }

    public void checkUpdata(JSONRequestListener<CheckUpdataResponse> checkUpdataListener) {
        stopCheckUpdata();

        HashMap<String, Object> params = new HashMap<String, Object>();

        checkUpdataRequest = NetRequestUtils.post().url(HttpConstants.getVersionUrl() + "method=findLatest").params(params).bulid();

        checkUpdataRequest.execute(checkUpdataListener);
    }

    public void stopCheckUpdata() {
        if (checkUpdataRequest != null) {
            checkUpdataRequest.cancel();
            checkUpdataRequest = null;
        }
    }
}
