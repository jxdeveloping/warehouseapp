package com.jxdeveloping.tool.Http;

import android.content.Context;

import com.framework.MidLayer_Net.Listener.JSONRequestListener;
import com.framework.MidLayer_Net.NetRequest;
import com.framework.MidLayer_Net.NetRequestUtils;
import com.lk.controller.Constants.HttpConstants;
import com.lk.controller.Http.BaseHttp;
import com.lk.controller.Model.Http.LoginResponse;

import java.util.HashMap;


public class UserHttp extends BaseHttp {
    private NetRequest userLoginNetRequest;

    public UserHttp(Context context) {
        super(context);
    }

    public void login(String userName, String password, JSONRequestListener<LoginResponse> loginRequestListener) {
        stopLogin();

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("username", userName);
        params.put("password", password);

        userLoginNetRequest = NetRequestUtils.post()
                .url(HttpConstants.getUrl() + "/auth/login")
                .params(params)
                .bulid();

        userLoginNetRequest.execute(loginRequestListener);
    }

    public void stopLogin() {
        if (userLoginNetRequest != null) {
            userLoginNetRequest.cancel();
            userLoginNetRequest = null;
        }
    }
}
