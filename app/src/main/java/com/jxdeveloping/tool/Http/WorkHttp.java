package com.jxdeveloping.tool.Http;

import android.content.Context;
import android.util.Log;

import com.framework.MidLayer_Net.Listener.JSONRequestListener;
import com.framework.MidLayer_Net.Model.ErrorMsg;
import com.framework.MidLayer_Net.Model.ErrorType;
import com.framework.MidLayer_Net.Model.NetResponse;
import com.framework.MidLayer_Net.NetRequest;
import com.framework.MidLayer_Net.NetRequestUtils;
import com.google.gson.Gson;
import com.lk.controller.Constants.HttpConstants;
import com.lk.controller.Http.BaseHttp;
import com.lk.controller.Model.Http.CheckOperCodeResponse;
import com.lk.controller.Model.Http.GetReciverTotalResponse;
import com.lk.controller.Model.Http.GetSaveTotalResponse;
import com.lk.controller.Model.Http.LabelImgResponse;
import com.lk.controller.Model.Http.LoadResponse;
import com.lk.controller.Model.Http.ProductInfoResponse;
import com.lk.controller.Model.Http.Response;
import com.lk.controller.Model.Http.SaveResponse;
import com.lk.controller.Model.Http.SendResponse;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;


public class WorkHttp extends BaseHttp {
    private NetRequest checkOperCodeNetRequest;
    private NetRequest sendNetRequest;

    public WorkHttp(Context context) {
        super(context);
    }

    /**
     * 验证客户编码
     *
     * @param token
     * @param operCode
     * @param checkOperCodeRequestListener
     */
    public void checkOperCode(String token, String operCode, JSONRequestListener<CheckOperCodeResponse> checkOperCodeRequestListener) {
        stopCheckOperCode();

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("token", token);
        params.put("operCode", operCode);

        checkOperCodeNetRequest = NetRequestUtils.post().tag(context).url(HttpConstants.getUrl() + "method=checkOperCode").params(params).bulid();

        checkOperCodeNetRequest.execute(checkOperCodeRequestListener);
    }

    private void stopCheckOperCode() {
        if (checkOperCodeNetRequest != null) {
            checkOperCodeNetRequest.cancel();
            checkOperCodeNetRequest = null;
        }
    }


    public void send(String token, String barcode, final JSONRequestListener<ProductInfoResponse> sendRequestListener) {
        Map<String,String> param=new HashMap<>();
        param.put("token", token);
        param.put("trackingNumber", barcode);

        String url = HttpConstants.getUrl() + "/warehouse/shipment";
        try {
            OkGo.post(url)
                    .params(param)
                    .tag(this).execute(new StringCallback() {
                @Override
                public void onSuccess(String s, Call call, okhttp3.Response response) {
                    Map<String,Object> tmpMap=new HashMap<>();
                    Map  retMap = new Gson().fromJson(s, Map.class);
                    if(retMap==null || retMap.containsKey("success") ){
                        tmpMap.put("success",false);
                        tmpMap.put("message","Not Found");
                    }else {
                        tmpMap.put("success",true);
                        tmpMap.put("message","Success");
                        tmpMap.put("map", retMap );
                    }
                    NetResponse netResponse = new NetResponse();
                    netResponse.setSuccess(true);
                    netResponse.setData(new Gson().toJson(tmpMap));
                    sendRequestListener.onResponse(netResponse);

                }

                @Override
                public void onError(Call call, okhttp3.Response response, Exception e) {
                    e.printStackTrace();
                    Map<String,Object> tmpMap=new HashMap<>();
                    tmpMap.put("success",false);
                    tmpMap.put("message","Request Failure");
                    NetResponse netResponse = new NetResponse();
                    netResponse.setSuccess(false);
                    netResponse.setData(new Gson().toJson(tmpMap));
                    sendRequestListener.onResponse(netResponse);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void stopSend() {
        if (sendNetRequest != null) {
            sendNetRequest.cancel();
            sendNetRequest = null;
        }
    }


}
