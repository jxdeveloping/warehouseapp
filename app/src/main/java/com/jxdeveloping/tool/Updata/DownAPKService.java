package com.jxdeveloping.tool.Updata;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.jxdeveloping.tool.BuildConfig;
import com.jxdeveloping.tool.R;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.FileCallback;
import com.lzy.okgo.request.BaseRequest;

import java.io.File;

import okhttp3.Call;
import okhttp3.Response;


public class DownAPKService extends Service {
    private static final int HANDLER_BEFORE = 0;
    private static final int HANDLER_SUCCESS = 1;
    private static final int HANDLER_ERROR = 2;
    private static final int HANDLER_CACHE_ERROR = 3;

    private final int NotificationID = 0x10000;
    private NotificationManager mNotificationManager = null;
    private NotificationCompat.Builder builder;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HANDLER_BEFORE: {
                    mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    builder = new NotificationCompat.Builder(getApplicationContext());
                    builder.setSmallIcon(R.mipmap.ic_launcher);
                    builder.setTicker("正在下载新版本");
                    builder.setContentTitle(getApplicationInfo().loadLabel(getPackageManager()).toString());
                    builder.setContentText("正在下载,请稍后...");
                    builder.setNumber(0);
                    builder.setAutoCancel(true);
                    mNotificationManager.notify(NotificationID, builder.build());
                    break;
                }
                case HANDLER_SUCCESS: {
                    // file 即为文件数据，文件保存在指定目录

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    //判断是否是AndroidN以及更高的版本
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        Uri contentUri = FileProvider.getUriForFile(DownAPKService.this, BuildConfig.APPLICATION_ID + ".fileProvider", (File) msg.obj);
                        intent.setDataAndType(contentUri, "application/vnd.android.package-archive");
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    } else {
                        Uri uri = Uri.fromFile((File) msg.obj);
                        intent.setDataAndType(uri, "application/vnd.android.package-archive");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }

                    PendingIntent mPendingIntent = PendingIntent.getActivity(DownAPKService.this, 0, intent, 0);

                    builder.setContentText("下载完成,请点击安装");
                    builder.setProgress(0, 0, false);
                    builder.setContentIntent(mPendingIntent);
                    mNotificationManager.notify(NotificationID, builder.build());

                    stopSelf();
                    startActivity(intent);// 下载完成之后自动弹出安装界面
                    break;
                }
                case HANDLER_ERROR: {
//                    mNotificationManager.cancel(NotificationID);
                    builder.setProgress(0, 0, false);
                    builder.setContentText("下载失败");
                    builder.setContentIntent(null);
                    mNotificationManager.notify(NotificationID, builder.build());
                    break;
                }
                case HANDLER_CACHE_ERROR: {
//                    mNotificationManager.cancel(NotificationID);
                    builder.setProgress(0, 0, false);
                    builder.setContentText("无法保存文件");
                    builder.setContentIntent(null);
                    mNotificationManager.notify(NotificationID, builder.build());
                    break;
                }
            }
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v(getClass().getName(), "onStartCommand onStartCommand onStartCommand");
        if (intent == null) {
            stopSelf();
        }

        System.out.println("onStartCommand");
        // 接收Intent传来的参数:
        String apkUrl = intent.getStringExtra("apkUrl");

        DownFile(apkUrl);

        return super.onStartCommand(intent, flags, startId);
    }

    private void DownFile(String file_url) {
        OkGo.post(file_url)//
                .tag(this)//
                .execute(new FileCallback() {  //文件下载时，可以指定下载的文件目录和文件名
                    @Override
                    public void onBefore(BaseRequest request) {
                        super.onBefore(request);

                        handler.obtainMessage(HANDLER_BEFORE).sendToTarget();
                    }

                    @Override
                    public void onSuccess(File file, Call call, Response response) {
                        Message message = handler.obtainMessage();
                        message.what = HANDLER_SUCCESS;
                        message.obj = file;
                        message.sendToTarget();
                    }

                    @Override
                    public void downloadProgress(long currentSize, long totalSize, float progress, long networkSpeed) {
                        //这里回调下载进度(该回调在主线程,可以直接更新ui)
                        builder.setProgress((int) totalSize, (int) currentSize, false);
                        builder.setContentInfo((int) (progress * 100) + "%");
                        mNotificationManager.notify(NotificationID, builder.build());
                    }

                    /** 请求失败，响应错误，数据解析错误等，都会回调该方法， UI线程 */
                    public void onError(Call call, Response response, Exception e) {
                        e.printStackTrace();
                        handler.obtainMessage(HANDLER_ERROR).sendToTarget();
                    }

                    /** 缓存失败的回调,UI线程 */
                    public void onCacheError(Call call, Exception e) {
                        handler.obtainMessage(HANDLER_CACHE_ERROR).sendToTarget();
                    }
                });

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stopSelf();
    }
}
