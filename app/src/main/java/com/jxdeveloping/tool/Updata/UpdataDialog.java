package com.jxdeveloping.tool.Updata;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lk.controller.Model.Version;
import com.lk.controller.Widget.WuLiuDialog;
import com.jxdeveloping.tool.R;


public class UpdataDialog extends WuLiuDialog {
    private TextView textView_nowVersion;
    private TextView textView_newVersion;
    private TextView textView_descript;

    private OnUpdataDialogClickListener onUpdataDialogClickListener;

    public UpdataDialog(@NonNull Context context, OnUpdataDialogClickListener onUpdataDialogClickListener) {
        super(context);
        this.onUpdataDialogClickListener = onUpdataDialogClickListener;

        textView_nowVersion = findViewById(R.id.textView_nowVersion);
        textView_newVersion = findViewById(R.id.textView_newVersion);
        textView_descript = findViewById(R.id.textView_descript);

        findViewById(R.id.button_confirm).setOnClickListener(onClickListener);
        findViewById(R.id.button_cancel).setOnClickListener(onClickListener);
    }

    public void setVersion(Version nowVersion, Version newVersion) {
        textView_nowVersion.setText(nowVersion.getVersionName());
        textView_newVersion.setText(newVersion.getVersionName());
        textView_descript.setText(newVersion.getNote());
    }

    @Override
    protected int getContentLayoutId() {
        return R.layout.dialog_updata;
    }

    @Override
    protected int getWidth() {
        return getContext().getResources().getDimensionPixelSize(R.dimen.dp250);
    }

    @Override
    protected int getHeight() {
        return ViewGroup.LayoutParams.WRAP_CONTENT;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int viewId = view.getId();
            if (viewId == R.id.button_confirm) {
                dismiss();
                onUpdataDialogClickListener.onUpdateNowClick();
            } else if (viewId == R.id.button_cancel) {
                dismiss();
                onUpdataDialogClickListener.onDontUpdateClick();
            }
        }
    };

    public interface OnUpdataDialogClickListener {
        public void onUpdateNowClick();
        public void onDontUpdateClick();
    }
}
