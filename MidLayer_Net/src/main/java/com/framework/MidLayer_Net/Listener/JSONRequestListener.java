package com.framework.MidLayer_Net.Listener;

import com.framework.MidLayer_Net.Model.ErrorMsg;
import com.framework.MidLayer_Net.Model.NetResponse;
import com.google.gson.Gson;

import java.lang.reflect.ParameterizedType;

/**
 * Created by LK on 2017/4/25.
 */

public abstract class JSONRequestListener<T> extends SimpleRequestListener {

    @Override
    public void onResponse(NetResponse netResponse) {
        if (netResponse.isSuccess()) {
            onJSONResponse((T) new Gson().fromJson(netResponse.getData(), getTClass()));
        } else {
            onError(netResponse.getCode(), netResponse.getErrorMsg());
        }
    }

    public abstract void onJSONResponse(T t);

    public abstract void onError(int code, ErrorMsg errorMsg);

    private Class getTClass() {
        return  (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
}
