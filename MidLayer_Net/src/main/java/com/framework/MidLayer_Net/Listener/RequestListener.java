package com.framework.MidLayer_Net.Listener;


import com.framework.MidLayer_Net.Model.NetResponse;

/**
 * Created by LK on 2017/3/15.
 */

public interface RequestListener {
    public void writeProgress(float percent, long totalSize);
    public void readProgress(float percent, long totalSize);
    public void onResponse(NetResponse netResponse);
}
