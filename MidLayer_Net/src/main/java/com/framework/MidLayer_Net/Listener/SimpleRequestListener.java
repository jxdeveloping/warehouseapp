package com.framework.MidLayer_Net.Listener;

/**
 * 一般请求Listener
 * Created by LK on 2017/3/15.
 */

public abstract class SimpleRequestListener implements RequestListener {
    @Override
    public void writeProgress(float percent, long totalSize) {

    }

    @Override
    public void readProgress(float percent, long totalSize) {

    }
}
