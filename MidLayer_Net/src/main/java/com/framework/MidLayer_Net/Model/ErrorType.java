package com.framework.MidLayer_Net.Model;

/**
 * Created by LK on 2017/3/15.
 */

public enum ErrorType {
    SERVER_ERROR, //服务端错误
    NET_ERROR  //网络连接错误
}
