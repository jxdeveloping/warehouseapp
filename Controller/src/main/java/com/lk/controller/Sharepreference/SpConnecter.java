package com.lk.controller.Sharepreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import java.io.IOException;

/**
 * Created by LK on 2017/3/16.
 */

public class SpConnecter {
    private Context context;
    private String fileName;

    public SpConnecter(Context context, @NonNull String fileName) {
        this.context = context;
        this.fileName = fileName;
    }

    /**
     * 保存
     * @param key
     * @param value
     */
    public void save(String key, String value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * 保存
     * @param key
     * @param value
     */
    public void save(String key, int value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putInt(key, value);
        editor.commit();
    }

    /**
     * 保存
     * @param key
     * @param value
     */
    public void save(String key, long value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putLong(key, value);
        editor.commit();
    }

    /**
     * 保存
     * @param key
     * @param value
     */
    public void save(String key, float value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putFloat(key, value);
        editor.commit();
    }

    /**
     * 保存
     * @param key
     * @param value
     */
    public void save(String key, boolean value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * 读取
     * @param key
     * @return
     */
    public String loadString(String key) {
        return loadString(key, null);
    }

    public String loadString(String key, String defValue) {
        return getsharedPreferences().getString(key, defValue);
    }

    /**
     * 读取
     * @param key
     * @return
     */
    public int loadInt(String key) {
        return loadInt(key, 0);
    }

    public int loadInt(String key, int defValue) {
        return getsharedPreferences().getInt(key, defValue);
    }

    /**
     * 读取
     * @param key
     * @return
     */
    public long loadLong(String key) {
        return loadLong(key, 0l);
    }

    /**
     * 读取
     * @param key
     * @return
     */
    public long loadLong(String key, long defValue) {
        return getsharedPreferences().getLong(key, defValue);
    }

    /**
     * 读取
     * @param key
     * @return
     */
    public float loadFloat(String key) {
        return loadFloat(key, 0f);
    }

    /**
     * 读取
     * @param key
     * @return
     */
    public float loadFloat(String key, float defValue) {
        return getsharedPreferences().getFloat(key, defValue);
    }

    /**
     * 读取
     * @param key
     * @return
     */
    public boolean loadBoolean(String key) {
        return loadBoolean(key, false);
    }

    public boolean loadBoolean(String key, boolean defValue) {
        return getsharedPreferences().getBoolean(key, defValue);
    }

    /**
     * 保存对象实例
     * @param object
     * @throws IOException
     */
    public void saveObject(Object object) {
        saveObject(object.getClass().getName(), object);
    }

    /**
     * 保存对象实例
     * @param object
     * @throws IOException
     */
    public void saveObject(String key, Object object) {
        save(key, new Gson().toJson(object));
    }

    /**
     * 读取对象实例
     * @param clazz
     * @param <T>
     * @return
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public <T> T loadObject(Class<T> clazz) {
        return loadObject(clazz.getName(), clazz);
    }

    /**
     * 读取对象实例
     * @param clazz
     * @param <T>
     * @return
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public <T> T loadObject(String key, Class<T> clazz) {
        return new Gson().fromJson(loadString(key), clazz);
    }

    /**
     * 删除Shareperference文件
     */
    public void delete() {
        getEditor().clear().commit();
    }

    /**
     * 获取Shareperference实例
     * @return
     */
    public SharedPreferences getsharedPreferences() {
        return context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
    }

    /**
     * 获取SharedPreferences.Editor实例
     * @return
     */
    public SharedPreferences.Editor getEditor() {
        return getsharedPreferences().edit();
    }
}
