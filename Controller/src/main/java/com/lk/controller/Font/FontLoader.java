package com.lk.controller.Font;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by LK on 2017/11/21.
 */

public class FontLoader {
    public static Typeface OCTICONS;

    /**
     * Get octicons typeface
     *
     * @param context
     * @return octicons typeface
     */
    public static Typeface getOcticons(final Context context, String ttfPath) {
        if (OCTICONS == null)
            OCTICONS = getTypeface(context, ttfPath);
        return OCTICONS;
    }

    /**
     * Set octicons typeface on given text view(s)
     *
     * @param textViews
     */
    public static void setOcticons(String ttfPath, final TextView... textViews) {
        if (textViews == null || textViews.length == 0)
            return;

        Typeface typeface = getOcticons(textViews[0].getContext(), ttfPath);
        for (TextView textView : textViews)
            textView.setTypeface(typeface);
    }

    /**
     * Get typeface with name
     *
     * @param context
     * @param name
     * @return typeface
     */
    public static Typeface getTypeface(final Context context, final String name) {
        return Typeface.createFromAsset(context.getAssets(), name);
    }
}
