package com.lk.controller.Widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.lk.controller.R;

/**
 * Created by LK on 2017/4/27.
 */

public class ProgressDialog extends Dialog {
    private ImageView imageView;

    private AnimationDrawable animationDrawable;

    public ProgressDialog(Context context){
        super(context, R.style.dialog_progress);

        setContentView(createContentView());
        getWindow().getAttributes().gravity = Gravity.CENTER;
    }

    private View createContentView() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setGravity(Gravity.CENTER);

        imageView = new ImageView(getContext());
        imageView.setBackgroundResource(R.drawable.anim_progress);
        linearLayout.addView(imageView);

        animationDrawable = (AnimationDrawable) imageView.getBackground();

        return linearLayout;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus){
        if (hasFocus) {
            animationDrawable.start();
        } else {
            animationDrawable.stop();
        }
    }
}
