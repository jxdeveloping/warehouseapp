package com.lk.controller.Widget;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.lk.controller.R;

/**
 * Created by LK on 2017/12/5.
 */

public abstract class WuLiuDialog extends Dialog {
    private View contentView;

    protected abstract int getContentLayoutId();

    protected abstract int getWidth();

    protected abstract int getHeight();

    public WuLiuDialog(@NonNull Context context) {
        super(context, R.style.dialog_notice);

        setCanceledOnTouchOutside(true);
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        contentView = layoutInflater.inflate(R.layout.dialog_wl, null);
        setContentView(contentView);
        getWindow().getAttributes().gravity = Gravity.CENTER;

        layoutInflater.inflate(getContentLayoutId(), (ViewGroup) contentView, true);

        View container = findViewById(R.id.container);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(getWidth(), getHeight());
        container.setLayoutParams(layoutParams);

        android.view.WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = getWidth();
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(lp);
    }
}
