package com.lk.controller.Model.Http;

import com.lk.controller.Model.LoadResult;

/**
 * Created by LK on 2017/11/22.
 */

public class LoadResponse extends Response {
    private LoadResult map;

    public LoadResult getMap() {
        return map;
    }

    public void setMap(LoadResult map) {
        this.map = map;
    }
}
