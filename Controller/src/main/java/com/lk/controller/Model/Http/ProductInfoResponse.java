package com.lk.controller.Model.Http;

import com.lk.controller.Model.ProductInfo;

/**
 * 产品返回信息.
 */
public class ProductInfoResponse extends Response{
    private ProductInfo map;

    public ProductInfo getMap() {
        return map;
    }

    public void setMap(ProductInfo map) {
        this.map = map;
    }
}
