package com.lk.controller.Model;

/**
 * Created by LK on 2017/12/5.
 */

public class Version {
    private String fileName; //下载链接
    private int versionCode; //版本号
    private String versionName; //版本名字
    private String tmCreate; //创建（发布）时间
    private String note; //说明

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getTmCreate() {
        return tmCreate;
    }

    public void setTmCreate(String tmCreate) {
        this.tmCreate = tmCreate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
