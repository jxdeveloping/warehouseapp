package com.lk.controller.Model;

/**
 * Created by LK on 2017/11/21.
 */

public class SaveTotal {
    private int TDNO; //客户当天取件后未入库总数
    private int PKUP; //客户取件总数
    private int PTIN; //客户入库总数
    private int ALLNO; //客户所有取件后未入库总数

    public int getTDNO() {
        return TDNO;
    }

    public void setTDNO(int TDNO) {
        this.TDNO = TDNO;
    }

    public int getPKUP() {
        return PKUP;
    }

    public void setPKUP(int PKUP) {
        this.PKUP = PKUP;
    }

    public int getPTIN() {
        return PTIN;
    }

    public void setPTIN(int PTIN) {
        this.PTIN = PTIN;
    }

    public int getALLNO() {
        return ALLNO;
    }

    public void setALLNO(int ALLNO) {
        this.ALLNO = ALLNO;
    }
}
