package com.lk.controller.Model.Http;

import com.lk.controller.Model.ReciverTotal;

/**
 * Created by LK on 2017/11/21.
 */

public class GetReciverTotalResponse extends Response {
    private ReciverTotal map;

    public ReciverTotal getMap() {
        return map;
    }

    public void setMap(ReciverTotal map) {
        this.map = map;
    }
}
