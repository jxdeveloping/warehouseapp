package com.lk.controller.Model.Http;

import com.lk.controller.Model.LabelImg;

/**
 * Created by YHY on 2018-03-09.
 */

public class LabelImgResponse  extends Response{
    private LabelImg map;

    public LabelImg getMap() {
        return map;
    }

    public void setMap(LabelImg map) {
        this.map = map;
    }
}
