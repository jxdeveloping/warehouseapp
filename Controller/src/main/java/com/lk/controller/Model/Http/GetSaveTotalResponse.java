package com.lk.controller.Model.Http;

import com.lk.controller.Model.SaveTotal;

/**
 * Created by LK on 2017/11/21.
 */

public class GetSaveTotalResponse extends Response {
    private SaveTotal map;

    public SaveTotal getMap() {
        return map;
    }

    public void setMap(SaveTotal map) {
        this.map = map;
    }
}
