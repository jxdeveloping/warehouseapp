package com.lk.controller.Model.Http;

import com.lk.controller.Model.UserCache;

/**
 * Created by LK on 2017/11/10.
 */

public class LoginResponse extends Response {
    private String token;;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserCache getUserCache(){
        return new UserCache(token);
    }
}
