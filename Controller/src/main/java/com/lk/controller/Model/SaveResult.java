package com.lk.controller.Model;

/**
 * Created by LK on 2017/11/22.
 */

public class SaveResult {
    private String time;
    private String agentCode;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }
}
