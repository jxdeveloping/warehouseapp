package com.lk.controller.Model.Http;

import com.lk.controller.Model.Version;

/**
 * Created by LK on 2017/12/5.
 */

public class CheckUpdataResponse extends Response {
    private Version map;

    public Version getMap() {
        return map;
    }

    public void setMap(Version map) {
        this.map = map;
    }
}
