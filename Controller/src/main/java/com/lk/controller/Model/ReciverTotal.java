package com.lk.controller.Model;

/**
 * Created by LK on 2017/11/21.
 */

public class ReciverTotal {
    private int NR; //当天有单取件数
    private int NP; //当天无单取件数

    public int getNR() {
        return NR;
    }

    public void setNR(int NR) {
        this.NR = NR;
    }

    public int getNP() {
        return NP;
    }

    public void setNP(int NP) {
        this.NP = NP;
    }
}
