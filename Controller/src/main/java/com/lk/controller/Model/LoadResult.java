package com.lk.controller.Model;

/**
 * Created by LK on 2017/11/22.
 */

public class LoadResult {
    private String agentCode; //客户编码
    private int totalN; //未出库总数

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public int getTotalN() {
        return totalN;
    }

    public void setTotalN(int totalN) {
        this.totalN = totalN;
    }
}
