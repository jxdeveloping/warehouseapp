package com.lk.controller.Model;

import java.io.Serializable;

/**
 * 客户
 * Created by LK on 2017/11/21.
 */

public class Oper implements Serializable {
    private String username;
    private String tel;
    private String operCode;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getOperCode() {
        return operCode;
    }

    public void setOperCode(String operCode) {
        this.operCode = operCode;
    }
}
