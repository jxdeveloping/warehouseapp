package com.lk.controller.Model.Http;

import java.util.Map;

public class SendResponse extends Response{

    private Map<String,Object> map;

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }
}
