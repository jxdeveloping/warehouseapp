package com.lk.controller.Model.Http;

import com.lk.controller.Model.Oper;

/**
 * Created by LK on 2017/11/21.
 */

public class CheckOperCodeResponse extends Response {
    private Oper map;

    public Oper getMap() {
        return map;
    }

    public void setMap(Oper map) {
        this.map = map;
    }
}
