package com.lk.controller.Model;

/**
 * Created by Administrator on 2017/11/6.
 */

public class UserCache {

    public UserCache() {

    }

    public UserCache(String token) {
        this.token = token;
    }

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
