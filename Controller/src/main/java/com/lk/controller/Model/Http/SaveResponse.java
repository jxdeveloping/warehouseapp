package com.lk.controller.Model.Http;

import com.lk.controller.Model.SaveResult;

/**
 * Created by LK on 2017/11/22.
 */

public class SaveResponse extends Response {
    private SaveResult map;

    public SaveResult getMap() {
        return map;
    }

    public void setMap(SaveResult map) {
        this.map = map;
    }
}
