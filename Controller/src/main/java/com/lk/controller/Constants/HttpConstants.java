package com.lk.controller.Constants;

/**
 * Created by LK on 2017/11/10.
 */

public class HttpConstants {
    private static String HTTP_URL = "http://61.216.178.44:8000";

    //private static final String HTTP_URL = "http://192.168.0.10:8080/expus";

    private static final String API = "/data-server";

    private static final String VERSION = "/systemVersion.do";

    public static final String getUrl() {
        return HTTP_URL + API;
    }

    public static void setHttpServerUrl(String url) {
        HTTP_URL = url;
    }

    public static final String getVersionUrl() {
        return HTTP_URL + VERSION + "?";
    }
}
