package com.lk.controller.Constants;

import android.support.annotation.StringDef;

/**
 * Created by LK on 2017/11/21.
 */
@StringDef({
        MDFonts.USERNAME,
        MDFonts.PASSWORD,
        MDFonts.RECIVER,
        MDFonts.SAVE,
        MDFonts.LOAD,
        MDFonts.SEND
})
public @interface MDFonts {
    String USERNAME = "\ue625";

    String PASSWORD = "\ue603";

    String RECIVER = "\ue69f";

    String SAVE = "\ue627";

    String LOAD = "\ue640";

    String SEND = "\ue724";
}
