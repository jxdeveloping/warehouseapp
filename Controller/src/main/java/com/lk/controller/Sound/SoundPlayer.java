package com.lk.controller.Sound;

import android.content.res.AssetManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Administrator on 2017/11/22.
 */

public class SoundPlayer {
    private static SoundPool soundPool;

    private static HashMap<String, Integer> path_idMap;

    public static void loadSound(AssetManager assetManager, String path) {
        try {
            Integer id = getPath_idMap().get(path);
            if (id == null || id == 0) {
                id = getSoundPool().load(assetManager.openFd(path), 1);
                getPath_idMap().put(path, id);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void playSound(AssetManager assetManager, String path) {
        try {
            Integer id = getPath_idMap().get(path);
            if (id == null || id == 0) {
                id = getSoundPool().load(assetManager.openFd(path), 1);
                getPath_idMap().put(path, id);
            }
            getSoundPool().play(id, 1, 1, 0, 0, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static SoundPool getSoundPool() {
        if (soundPool == null) {
            //当前系统的SDK版本大于等于21(Android 5.0)时
            if (Build.VERSION.SDK_INT >= 21) {
                SoundPool.Builder builder = new SoundPool.Builder();
                //传入音频数量
                builder.setMaxStreams(2);
                //AudioAttributes是一个封装音频各种属性的方法
                AudioAttributes.Builder attrBuilder = new AudioAttributes.Builder();
                //设置音频流的合适的属性
                attrBuilder.setLegacyStreamType(AudioManager.STREAM_MUSIC);
                //加载一个AudioAttributes
                builder.setAudioAttributes(attrBuilder.build());
                soundPool = builder.build();
            }
            //当系统的SDK版本小于21时
            else {//设置最多可容纳2个音频流，音频的品质为5
                soundPool = new SoundPool(2, AudioManager.STREAM_SYSTEM, 5);
            }
            soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                @Override
                public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {

                }
            });
        }

        return soundPool;
    }

    private static HashMap<String, Integer> getPath_idMap() {
        if (path_idMap == null) {
            path_idMap = new HashMap<String, Integer>();
        }
        return path_idMap;
    }

    private static void release() {
        getSoundPool().autoResume();
        getPath_idMap().clear();
    }
}
