package com.lk.controller.Page;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.view.inputmethod.InputMethodManager;

import com.lk.controller.Constants.ActivityRequestCode;
import com.lk.controller.Constants.SoundPath;
import com.lk.controller.Constants.SpConstants;
import com.lk.controller.Model.UserCache;
import com.lk.controller.Sharepreference.SpConnecter;
import com.lk.controller.Sound.SoundPlayer;
import com.lk.controller.Widget.ProgressDialog;

import me.yokeyword.fragmentation.SupportFragment;

/**
 * Created by LK on 2017/11/1.
 */

public abstract class BaseFragment extends SupportFragment {
    private ProgressDialog progressDialog;

    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) _mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(_mActivity.getWindow().getDecorView().getWindowToken(), 0);
    }

    /**
     * 显示progress dialog
     */
    protected void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(_mActivity);
        }
        progressDialog.show();
    }

    /**
     * 隐藏progress dialog
     */
    protected void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    /**
     * 显示snaker bar
     * @param message
     */
    public void showSnakerBar(String message) {
        showSnakerBar(message, Snackbar.LENGTH_SHORT);
    }

    /**
     * 显示snaker bar
     * @param message
     * @param duration
     */
    public void showSnakerBar(String message, int duration) {
        Snackbar.make(_mActivity.getWindow().getDecorView().findViewById(android.R.id.content), message, duration).show();
    }

    /**
     * 获取token
     * @return
     */
    public String getToken() {
        SpConnecter spConnecter = new SpConnecter(_mActivity, SpConstants.USER_CACHE);
        UserCache userCache = spConnecter.loadObject(UserCache.class);
        if (userCache != null) {
            return userCache.getToken();
        }
        return null;
    }

    /**
     * 保存token
     * @param userCache
     */
    public void saveToken(UserCache userCache) {
        SpConnecter spConnecter = new SpConnecter(_mActivity, SpConstants.USER_CACHE);
        if (userCache == null) {
            spConnecter.delete();
        } else {
            spConnecter.saveObject(userCache);
        }
    }

    /**
     * 登录
     * @param loginActivityClass
     */
    public void gotoLogin(Class loginActivityClass) {
        startActivityForResult(new Intent(_mActivity, loginActivityClass), ActivityRequestCode.LOGIN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ActivityRequestCode.LOGIN) {
            if (resultCode != RESULT_OK) {
                onLoginFailedCB();
            }
        }
    }

    /**
     * 登录失败回调
     */
    public void onLoginFailedCB() {

    }

    public void playSuccessSound() {
        SoundPlayer.playSound(_mActivity.getAssets(), SoundPath.SOUND_SUCCESS);
    }

    public void playFailedSound() {
        SoundPlayer.playSound(_mActivity.getAssets(), SoundPath.SOUND_FAILED);
    }
}
