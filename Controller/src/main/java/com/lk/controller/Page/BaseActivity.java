package com.lk.controller.Page;

import android.content.Intent;
import android.support.design.widget.Snackbar;

import com.lk.controller.Constants.ActivityRequestCode;
import com.lk.controller.Constants.SoundPath;
import com.lk.controller.Constants.SpConstants;
import com.lk.controller.Model.UserCache;
import com.lk.controller.Sharepreference.SpConnecter;
import com.lk.controller.Sound.SoundPlayer;
import com.lk.controller.Widget.ProgressDialog;

import me.yokeyword.fragmentation.SupportActivity;

/**
 * Created by LK on 2017/11/1.
 */

public abstract class BaseActivity extends SupportActivity {

    private ProgressDialog progressDialog;

    @Override
    public void onResume() {
        super.onResume();
        if (checkLogin() && getToken() == null) {
            finish();
        }
    }

    /**
     * 是否开启登录检查
     * @return
     */
    public boolean checkLogin() {
        return true;
    }

    /**
     * 显示progress dialog
     */
    protected void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
        }
        progressDialog.show();
    }

    /**
     * 隐藏progress dialog
     */
    protected void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    /**
     * 显示snaker bar
     * @param message
     */
    public void showSnakerBar(String message) {
        showSnakerBar(message, Snackbar.LENGTH_SHORT);
    }

    /**
     * 显示snaker bar
     * @param message
     * @param duration
     */
    public void showSnakerBar(String message, int duration) {
        Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), message, duration).show();
    }

    /**
     * 获取token
     * @return
     */
    public String getToken() {
        SpConnecter spConnecter = new SpConnecter(this, SpConstants.USER_CACHE);
        UserCache userCache = spConnecter.loadObject(UserCache.class);
        if (userCache != null) {
            return userCache.getToken();
        }
        return null;
    }

    /**
     * 清除token
     */
    public void clearToken() {
        SpConnecter spConnecter = new SpConnecter(this, SpConstants.USER_CACHE);
        spConnecter.delete();
    }

    /**
     * 登录
     * @param loginActivityClass
     */
    public void gotoLogin(Class loginActivityClass) {
        startActivityForResult(new Intent(this, loginActivityClass), ActivityRequestCode.LOGIN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ActivityRequestCode.LOGIN) {
            if (resultCode != RESULT_OK) {
                onLoginFailedCB();
            }
        }
    }

    public void onLoginFailedCB() {

    }

    public void playSuccessSound() {
        SoundPlayer.playSound(getAssets(), SoundPath.SOUND_SUCCESS);
    }

    public void playFailedSound() {
        SoundPlayer.playSound(getAssets(), SoundPath.SOUND_FAILED);
    }
}
